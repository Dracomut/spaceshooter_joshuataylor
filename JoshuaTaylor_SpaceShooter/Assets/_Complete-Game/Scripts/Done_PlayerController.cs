﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Done_Boundary
{
    public float xMin, xMax, zMin, zMax;
}

public class Done_PlayerController : MonoBehaviour
{
    public float speed;
    public float tilt;
    public Done_Boundary boundary;

    public GameObject shot;
    public Transform shotSpawn;
    public float fireRate;

    public float chargeAmount = 0f;
    public float chargeRate;

    public AudioClip lowCharge;
    public AudioClip medCharge;
    public AudioClip maxCharge;
    public GameObject chargeEffect;

    private float nextFire;
    private float time;

    void Update()
    {

        gameObject.GetComponentInChildren<Light>().range = chargeAmount * 0.5f;
        gameObject.GetComponentInChildren<Light>().intensity = chargeAmount;

        print(chargeAmount);
        /*if (Input.GetButton("Fire1") && Time.time > nextFire) 
		{
			nextFire = Time.time + fireRate;                                // Fire Rate
			Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
			GetComponent<AudioSource>().Play ();
		}*/
        if (Input.GetButtonDown("Fire1"))
        {
            chargeAmount = 1f;
        }

        if (Input.GetButton("Fire1")) //Possibly change to Fire 2
        {
            if (chargeAmount <= 5f)
            {

                chargeAmount += chargeRate * Time.deltaTime;

            }
            else
            {
                chargeAmount = 5f;
            }

        }

        if (Input.GetButtonUp("Fire1"))
        {
            GameObject projectile = Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
            projectile.GetComponent<Done_Mover>().chargeAmount = chargeAmount;
            GetComponent<AudioSource>().Play();
            chargeAmount = 0;
        }
        if (chargeAmount > 1)
        {
            chargeEffect.SetActive(true);
        }
        else                               //PARTICLE EFFECT TOGGLE
        {
            chargeEffect.SetActive(false);
        }

        if (chargeAmount >= 1 && chargeAmount < 3 && gameObject.GetComponent<AudioSource>().isPlaying == false)
        {
            gameObject.GetComponent<AudioSource>().clip = lowCharge;
        }
        else if (chargeAmount >= 3 && chargeAmount < 5)
        {
            gameObject.GetComponent<AudioSource>().clip = medCharge;                                               //SOUND TOGGLER
        }
        else if (chargeAmount >= 5)
        {
            gameObject.GetComponent<AudioSource>().clip = maxCharge;
        }
        else
        {
            return;
        }

    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        GetComponent<Rigidbody>().velocity = movement * speed;

        GetComponent<Rigidbody>().position = new Vector3
        (
            Mathf.Clamp(GetComponent<Rigidbody>().position.x, boundary.xMin, boundary.xMax),
            0.0f,
            Mathf.Clamp(GetComponent<Rigidbody>().position.z, boundary.zMin, boundary.zMax)
        );

        GetComponent<Rigidbody>().rotation = Quaternion.Euler(0.0f, 0.0f, GetComponent<Rigidbody>().velocity.x * -tilt);
    }
}
