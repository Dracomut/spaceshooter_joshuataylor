﻿using UnityEngine;
using System.Collections;

public class Done_Mover : MonoBehaviour
{
	public float speed;
    public float chargeAmount;

	void Start ()
	{
		GetComponent<Rigidbody>().velocity = transform.forward * speed;
	}

    private void Update()
    {
        gameObject.transform.localScale = new Vector3(chargeAmount, chargeAmount, chargeAmount);

        if (chargeAmount < 1)
        {
            Destroy(gameObject);
        }
    }
}
